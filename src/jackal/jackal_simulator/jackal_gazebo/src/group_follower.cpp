#include <ros/ros.h>
#include <pluginlib/class_list_macros.h>
#include <geometry_msgs/Twist.h>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/PoseStamped.h>
#include <sensor_msgs/LaserScan.h>
#include <people_msgs/People.h>
#include <spencer_tracking_msgs/TrackedPersons.h>
#include <spencer_tracking_msgs/TrackedGroups.h>
#include <std_msgs/Empty.h>
#include <topic_tools/MuxSelect.h>
#include <tf/tf.h>
#include <tf/tfMessage.h>
#include <tf/transform_listener.h>
#include <nodelet/nodelet.h>
#include <message_filters/subscriber.h>
#include <message_filters/time_synchronizer.h>
#include <message_filters/sync_policies/approximate_time.h>
#include <message_filters/cache.h>
#include <cmath> 
#include <limits>


/**
  group_follower.cpp
  Purpose: Subscribes to people and detects nearby people, then publishes a cmd_vel to move robot to follow detected crowd 
**/


namespace group_follower{

//Class for each detected group to store the average velocity, position, and closest person
class PeopleGroup{
public:
   PeopleGroup() {   }
   ~PeopleGroup() {   }
 
   float avg_x_pos=0;
   float avg_y_pos=0;
   float avg_vel=0;
   float closest_person_x=0;
   float closest_person_y=0;
  
   void setAverages(float a_x_pos, float a_y_pos, float a_vel, float c_person_x, float c_person_y) {
     avg_x_pos = a_x_pos;
     avg_y_pos = a_y_pos;
     avg_vel = a_vel;
     closest_person_x = c_person_x;
     closest_person_y = c_person_y;
   }

   PeopleGroup(const PeopleGroup& other){
     avg_x_pos = other.avg_x_pos;
     avg_y_pos = other.avg_y_pos;
     avg_vel = other.avg_vel;
     closest_person_x = other.closest_person_x;
     closest_person_y = other.closest_person_y;
   }

   PeopleGroup& operator= (const PeopleGroup& other){
     if(this != &other){
     avg_x_pos = other.avg_x_pos;
     avg_y_pos = other.avg_y_pos;
     avg_vel = other.avg_vel;
     closest_person_x = other.closest_person_x;
     closest_person_y = other.closest_person_y;
     }
     return *this;
   }
};

class GroupFollower : public nodelet::Nodelet
{
public:
  GroupFollower() : follow_distance_(5.0), stop_distance_(1), speed_scale_(0.75), p_angle_gain_(5), i_angle_gain_(5), max_linear_(2), max_angular_(2), desired_vel_(2)
  {
  }

  ~GroupFollower()
  {
  }
private:
  double follow_distance_; //follow objects within this radius of robot
  double stop_distance_; //margin of space between self and object being followed
  double speed_scale_; //constant to scale speed by (proportional control)
  double p_angle_gain_; //constant to scale turning by (proportional control)
  double i_angle_gain_; //constant to scale turning by (integral control)
  double max_linear_; //max linear velocity
  double max_angular_; //max angular velocity
  double desired_vel_; //robot desired velocity 
 
  float closest_person_ = 0; //distance to closest person (m)  
  float prev_x_ = 0; //store past linear x velocity
  float prev_z_ = 0; //store past angular z velocity
  float follow_dist_error_ = 0; //error between closest_person_ and stop_distance_
  float angle_error_ = 0; //angular error for integral control
 
  float r_x_pos = 0; //Robot x position 
  float r_y_pos = 0; //Robot y position 
  float r_angle = 0; //Robot yaw

  //goal setting  
  float g_x_pos = 0;
  float g_y_pos = 0;
  float g_angle = 0;
  float g_magnitude = 0;
  int dominant_axis = 1; //1 for x axis, 0 for y


  //Synchronizing group and people detection
  ros::NodeHandle follow_nh;
  message_filters::Subscriber<spencer_tracking_msgs::TrackedPersons> people_sub_;
  message_filters::Subscriber<spencer_tracking_msgs::TrackedGroups> group_sub_;  
  typedef message_filters::sync_policies::ApproximateTime<spencer_tracking_msgs::TrackedPersons, spencer_tracking_msgs::TrackedGroups> syncPolicy;
  typedef message_filters::Synchronizer<syncPolicy> sync;
  boost::shared_ptr<sync> sync_;

  tf::TransformListener listener;
  ros::Publisher cmd_pub_;
  ros::Publisher goal_pub_;
  ros::Subscriber goal_sub_;
  ros::Publisher group_goal_pub_;
  
  topic_tools::MuxSelect sel_req;


  virtual void onInit()
  {
    ros::NodeHandle& nh = getNodeHandle();
    ros::NodeHandle& private_nh = getPrivateNodeHandle();
 
    private_nh.getParam("follow_distance", follow_distance_);
    private_nh.getParam("stop_distance", stop_distance_);
    private_nh.getParam("speed_scale", speed_scale_);
    private_nh.getParam("p_angle_gain", p_angle_gain_);
    private_nh.getParam("i_angle_gain", i_angle_gain_);
    private_nh.getParam("max_linear", max_linear_);
    private_nh.getParam("max_angular", max_angular_);
    private_nh.getParam("desired_vel", desired_vel_);
   
    //Subscribe to people and group tracking data
    people_sub_.subscribe(follow_nh, "spencer/perception/tracked_persons", 1);
    group_sub_.subscribe(follow_nh, "spencer/perception/tracked_groups", 1, ros::TransportHints().tcpNoDelay());      sync_.reset(new sync(syncPolicy(10), people_sub_, group_sub_)); 
    sync_->registerCallback(boost::bind(&GroupFollower::peoplecb, this, _1, _2)); 
    
    //Subscribe to goal position
    goal_sub_ = nh.subscribe<geometry_msgs::Pose>("waypoint", 1, &GroupFollower::goalcb, this);
   
    //Publish movement commands to base
    cmd_pub_ = nh.advertise<geometry_msgs::Twist>("cmd_vel",1);

    //Publish success
    goal_pub_ = nh.advertise<std_msgs::Empty>("sm_arrive", 1);

    //Publish group location to follow
    group_goal_pub_ = nh.advertise<geometry_msgs::PoseStamped>("group_goal", 1);
  
    //Publish whether or not there are people
   // people_flag_pub_ = nh.advertise<std_msgs::Bool>("people_flag", 1);
  }


  void goalcb(const geometry_msgs::Pose::ConstPtr& goal){
    g_x_pos = goal->position.x; // - r_x_pos;
    g_y_pos = goal->position.y; // - r_y_pos;
 
    g_angle = getTheta(g_x_pos, g_y_pos) + r_angle;
    g_magnitude = getLen(g_x_pos, g_y_pos);
    g_x_pos = g_magnitude*cos(g_angle);
    g_y_pos = g_magnitude*sin(g_angle);
//    g_angle = getTheta(g_x_pos, g_y_pos) - r_angle;
//   ROS_ERROR("r_x_pos %.3f, r_y_pos %.3f", r_x_pos, r_y_pos);
//    ROS_ERROR("r_angle %.3f, g_angle %.3f", r_angle, g_angle);
    //Angle conversions
 /*   if(g_angle > M_PI){
        g_angle = (g_angle - 2*M_PI);
        if(g_y_pos < 0) g_angle = g_angle * -1;
    }
    else if(g_angle < -M_PI){
        g_angle = -(g_angle + 2*M_PI);
        if(g_y_pos < 0) g_angle = g_angle * -1;
    }

    g_x_pos = g_magnitude*cos(g_angle);
    g_y_pos = g_magnitude*sin(g_angle);
*/
/*    if(g_magnitude < 1){
      std_msgs::Empty success;
      goal_pub_.publish(success);
      ROS_ERROR("Reached goal"); 
      g_x_pos = 0;
      g_y_pos = 0;
    }
 */
    if(fabs(g_x_pos) < fabs(g_y_pos)){
 //   if(fabs(g_angle) > M_PI/4 && fabs(g_angle) < 3*M_PI/4 ){ //goal is closer to y axis than x 
      dominant_axis = 0;
    }
    else{
      dominant_axis = 1;
    }

  }
 
  void peoplecb(const spencer_tracking_msgs::TrackedPersons::ConstPtr& people, const spencer_tracking_msgs::TrackedGroups::ConstPtr& groups)
  {
    closest_person_ = 0;
    int num_people = people->tracks.size();
    int num_groups = groups->groups.size();

    //deal with transforming from odom to base_link frames
    tf::StampedTransform transform;
    try{
       ros::Time now = ros::Time::now();
       listener.waitForTransform("/odom", "/base_link", now, ros::Duration(5.0));
       listener.lookupTransform("/odom", "/base_link", now, transform);
       r_x_pos = transform.getOrigin().x();
       r_y_pos = transform.getOrigin().y();
       r_angle = tf::getYaw(transform.getRotation());
    } catch (tf::TransformException ex) {
       ROS_ERROR("%s", ex.what());
       return;
    }   

    std::vector<PeopleGroup> groups_info; 

    //find average position and velocity of each group
    for(auto group : groups->groups){
      PeopleGroup curGroup;
      float group_vel = 0;
      float group_closest=0;
      float group_x = 0;
      float group_y = 0;
      float group_closest_x = 0;
      float group_closest_y = 0;
      int group_size = group.track_ids.size();
      for(int track_id : group.track_ids){
     	for(auto track : people->tracks){
          if(track.track_id == track_id){  //then this track is in the group
            float x_vel = track.twist.twist.linear.x;
            float y_vel = track.twist.twist.linear.y;
            float vel = getLen(x_vel, y_vel); 
            
            if(vel < .4) {
                group_size--;
                continue;
            }
            float turn_angle = getTheta(x_vel, y_vel);
            //ROS_ERROR("g_x_p: %.3f, g_y_p: %.3f", g_x_pos, g_y_pos);
            //ROS_ERROR("x_vel: %.3f, y_vel: %.3f", x_vel, y_vel);
            //ROS_ERROR("Track ID: %d, Turn angle: %.3f, R_angle: %.3f", track_id, turn_angle, r_angle);
            if(fabs(turn_angle - r_angle) > M_PI*2/3 && fabs(turn_angle - r_angle) < M_PI*4/3){
                group_size--;
                continue; //going in opposite direction, don't follow
            }
            float g_mag = getLen(g_x_pos, g_y_pos);
            float dot_ang = acos((x_vel*g_x_pos + y_vel*g_y_pos)/(g_mag * vel));
            ROS_ERROR("angle: %.3f", dot_ang);
            if(fabs(dot_ang) >= 1.4){ 
            //if(x_vel*g_x_pos + y_vel*g_y_pos <= 0){ 
               group_size--;
                 continue;
            }
 
            group_vel += vel; //cumulative group velocity
            group_x += track.pose.pose.position.x;
            group_y += track.pose.pose.position.y;

            float cur_dist = getLen(track.pose.pose.position.x - r_x_pos, track.pose.pose.position.y - r_y_pos);
            if(cur_dist < closest_person_ || closest_person_ == 0){
              closest_person_ = cur_dist; 
            }
            if(cur_dist < group_closest || group_closest ==0){
              group_closest = cur_dist;
              group_closest_x = track.pose.pose.position.x - r_x_pos;
              group_closest_y = track.pose.pose.position.y - r_y_pos;
            }
          }
        }
      }
    
      if(!group_size) continue;
      curGroup.setAverages(group_x/group_size - r_x_pos, group_y/group_size - r_y_pos, group_vel/group_size, group_closest_x, group_closest_y);
      groups_info.push_back(curGroup);
    }
    
    //find optimal group to follow
    int optimal_group = -1;
    float vel_error = std::numeric_limits<float>::infinity(); 
 
    if(groups_info.size() > 0 && closest_person_ >= stop_distance_){
      for(int i = 0; i < groups_info.size(); i++){
        if(fabs(groups_info[i].avg_vel - desired_vel_) < vel_error){ 
          optimal_group = i; //find group moving closest to desired velocity
          vel_error = fabs(groups_info[i].avg_vel - desired_vel_);
        }
      }
      if(optimal_group < 0) return; //no groups to follow
     // ROS_ERROR("x_pos: %3f, y_pos: %3f, avg_vel: %3f, closest: %3f", groups_info[optimal_group].avg_x_pos, groups_info[optimal_group].avg_y_pos, groups_info[optimal_group].avg_vel, groups_info[optimal_group].closest_person);
      float group_closest_person_x = groups_info[optimal_group].closest_person_x;
      float group_closest_person_y = groups_info[optimal_group].closest_person_y;
      float group_closest_person = getLen(group_closest_person_x, group_closest_person_y);

      if(group_closest_person >= stop_distance_){
        follow_dist_error_ = group_closest_person - stop_distance_;
        float x_pos = groups_info[optimal_group].avg_x_pos;
        float y_pos = groups_info[optimal_group].avg_y_pos;
        float turn_angle = getTheta(x_pos, y_pos);
      //  float diff = turn_angle - r_angle;
        

        float diff = getTheta(group_closest_person_x, group_closest_person_y) - r_angle;        

        //Angle conversions
        if(diff > M_PI){
          diff = (diff - 2*M_PI);
          if(y_pos < 0) diff = diff * -1;
        }
        else if(diff < -M_PI){
          diff = -( diff + 2*M_PI);
          if(y_pos < 0) diff = diff * -1;
        }


        sel_req.request.topic = "/group_goal";
        ros::service::call("mux_goal/select", sel_req);
        pubGroupGoal(group_closest_person*cos(diff), group_closest_person*sin(diff));
     //  follow(groups_info[optimal_group].avg_vel, diff);      
      }
    }
    else{
        sel_req.request.topic = "/curb_goal";
        ros::service::call("mux_goal/select", sel_req);
    //  stop();
    }
  }  

  void pubGroupGoal(float goal_x, float goal_y){
    geometry_msgs::PoseStamped goal;
    goal.header.frame_id = "base_link";
    goal.pose.position.x = goal_x;
    goal.pose.position.y = goal_y;

    group_goal_pub_.publish(goal);
  }


  float getTheta(float x, float y){
    float turn_angle;
    if(x > 0){
      turn_angle = atan(y/x);
    }
    else if(y > 0){
      turn_angle = M_PI + atan(y/x);
    }
    else{
      turn_angle = -M_PI + atan(y/x);
    }
    return turn_angle;
  }

  bool withinRange(float val_1, float val_2, float range){
    return (fabs(val_1 - val_2) <= range); 
  } 
  
  float radToDegrees(float rad){
    return rad * 180 / M_PI; 
  }

  float getLen(float x, float y)
  {
    return sqrt(x*x + y*y);
  }
  
  // Publishes to cmd_vel to move robot towards detected object
  void follow(float avg_vel, float  avg_angle)
  {
    geometry_msgs::TwistPtr cmd(new geometry_msgs::Twist());
    if(fabs(follow_dist_error_) <= 0.1 || fabs(radToDegrees(avg_angle)) > 70){ //if we are within 10cm of stop_distance or turning, mimic velocity of group
    	cmd->linear.x = avg_vel; 
    }
    else{ //catch up to person
        cmd->linear.x = speed_scale_ * follow_dist_error_ ;
    }
    if(fabs(radToDegrees(avg_angle)) < 40){ //if not turning
      angle_error_ += avg_angle;
    }
    cmd->angular.z = angle_error_ * i_angle_gain_ + avg_angle * p_angle_gain_ * follow_dist_error_;//the further we are, the faster we turn

    if(cmd->linear.x > max_linear_){
      cmd->linear.x = max_linear_;
    }
    else if(cmd->linear.x < -1*max_linear_){
      cmd->linear.x = -1*max_linear_;
    }

    if(cmd->angular.z > max_angular_){
      cmd->angular.z = max_angular_;
    }
    else if(cmd->angular.z < -1*max_angular_){
      cmd->angular.z = -1*max_angular_;
    }

    cmd_pub_.publish(cmd);
    prev_x_ = cmd->linear.x;
    prev_z_ = cmd->angular.z;
  }

  // stop all movement
  void stop()
  {
    geometry_msgs::TwistPtr cmd(new geometry_msgs::Twist());
    cmd->linear.x = prev_x_/1.4;
    cmd->angular.z = prev_z_/4;
    cmd_pub_.publish(cmd);
     
    prev_x_ = cmd->linear.x;
    prev_z_ = cmd->angular.z;
  }
};

PLUGINLIB_EXPORT_CLASS(group_follower::GroupFollower, nodelet::Nodelet)
}
