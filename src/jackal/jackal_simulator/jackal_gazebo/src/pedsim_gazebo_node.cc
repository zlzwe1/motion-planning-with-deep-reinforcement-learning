// spawn same number of gazebo models as there are agents simulated by pedsim_ros, then shutdown

#include "ros/ros.h"
#include "ros/package.h"

#include "gazebo_msgs/SpawnModel.h"
#include "pedsim_msgs/AgentStates.h"
#include "pedsim_msgs/AgentState.h"
#include "std_msgs/String.h"
#include "geometry_msgs/Twist.h"
#include <string>

#include <streambuf>
#include <fstream>

bool first_msg = true;

// from rubble/random_spawn.cpp
std::string loadModelSDF(const std::string file)
{
  std::string filename;
  try
  {
    if (file.find("package://") == 0)
    {
      filename = file;
      filename.erase(0, strlen("package://"));
      size_t pos = filename.find("/");
      if (pos != std::string::npos)
      {
        std::string package = filename.substr(0, pos);
        filename.erase(0, pos);
        std::string package_path = ros::package::getPath(package);
        filename = package_path + filename;
      }
    }
    else
    {
      ROS_ERROR("Failed to locate file: %s", file.c_str());
      return "";
    }
  }
  catch (std::exception& e)
  {
    ROS_ERROR("Failed to retrieve file: %s", e.what());
    return "";
  }

  std::ifstream t(filename);
  std::string outstr;

  t.seekg(0, std::ios::end);
  outstr.reserve(t.tellg());
  t.seekg(0, std::ios::beg);

  outstr.assign((std::istreambuf_iterator<char>(t)),
           std::istreambuf_iterator<char>());

  return outstr;
} // end loadModelSDF()

void spawnModels(const pedsim_msgs::AgentStatesConstPtr &msg)
{
  ros::NodeHandle node; // local
  ros::ServiceClient client =
    node.serviceClient<gazebo_msgs::SpawnModel>("/gazebo/spawn_sdf_model" , false);
  ROS_INFO_STREAM(ros::this_node::getName() + "waiting for " + client.getService());
  client.waitForExistence();
  gazebo_msgs::SpawnModel srv;

  srv.request.model_xml = loadModelSDF("package://pm_robot/models/person_standing/model_set_pose.sdf");
  srv.request.reference_frame = "world";
  
  for(int i=0; i<msg->agent_states.size(); i++)
  {
    srv.request.model_name = std::to_string(msg->agent_states[i].id);
    srv.request.robot_namespace = std::to_string(msg->agent_states[i].id);
    srv.request.initial_pose = msg->agent_states[i].pose;
    // check whether service exists
    if (!client.waitForExistence(ros::Duration(2.0))){
      ROS_FATAL_STREAM(
        "Service '" << client.getService() << "' is not advertised and available.");
      break;
    } // end if
    else{// call service
      if (client.call(srv)){
        ROS_INFO_STREAM("Spawn response: "+ srv.response.status_message);
      } // end if
      else{
        ROS_FATAL_STREAM("Failed to call service '" + client.getService() + "'.");
      } // end else
    } // end else

  } // end for
  ROS_INFO("Spawning complete");

} // end spawnModels()

void callbackSimulatedAgents(const pedsim_msgs::AgentStatesConstPtr &msg)
{
  if(first_msg){ // spawn models
    ROS_INFO("First msg, spawn modelz");
    spawnModels(msg);
    //ROS_INFO_STREAM(msg);
    first_msg = false;
  }
  ros::shutdown(); // shutdown after spawning
} // end callbackSimulatedAgents()

int main(int argc, char **argv)
{
  
  ros::init(argc, argv, "pedsim_gazebo_node");
  ros::NodeHandle node;
  ROS_INFO_STREAM("Starting " + ros::this_node::getName());

  //TODO check whether topic exists / whether I've successfully subscribed
  ros::Subscriber sub = node.subscribe(
  		"/pedsim_simulator/simulated_agents", 1, callbackSimulatedAgents);
  ROS_INFO_STREAM(ros::this_node::getName() + " up and running !");

  ros::spin();
  return 0;
} // end main()
