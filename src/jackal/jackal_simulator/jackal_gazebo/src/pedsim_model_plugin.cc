#ifndef _PEDSIM_MODEL_PLUGIN_HH_
#define _PEDSIM_MODEL_PLUGIN_HH_

#include <gazebo/gazebo.hh>
#include <gazebo/physics/physics.hh>
#include <gazebo/common/common.hh>
#include <gazebo/math/gzmath.hh>

#include <math.h>
#include <tf/tf.h>
#include <boost/bind.hpp>
#include <stdio.h>

//ROS
#include <thread>
#include "ros/ros.h"
#include "ros/callback_queue.h"
#include "ros/subscribe_options.h"
 
#include "geometry_msgs/Pose.h"
#include "pedsim_msgs/TrackedPersons.h"

namespace gazebo{

	class PedsimModelPlugin : public ModelPlugin
	{
		/// \brief Constructor
		public: PedsimModelPlugin(){}

		/// \brief Load plugin, subscribe to /pedsim_visualizer/tracked_persons
	    /// \param[in] _model A pointer to the model that this plugin is
	    /// attached to.
	    /// \param[in] _sdf A pointer to the plugin's SDF element.
	    public: virtual void Load(physics::ModelPtr _model, sdf::ElementPtr _sdf)
    	{

		  // Store the model pointer for convenience.
	      this->model = _model;

	      // Initialize ros, if it has not already been initialized.
	      if (!ros::isInitialized())
	      {
	        int argc = 0;
	        char **argv = NULL;
	        ros::init(argc, argv, "pm_robot_client",
	            ros::init_options::NoSigintHandler);
	      }

	     // create ROS node	     
	     this->rosNode.reset(new ros::NodeHandle("pm_robot_client"));
	    
	    // subscribe  	
      	ros::SubscribeOptions so =
        	ros::SubscribeOptions::create<pedsim_msgs::TrackedPersons>(
	            "/pedsim_visualizer/tracked_persons",
	            1,
	            boost::bind(&PedsimModelPlugin::OnRosMsg, this, _1),
	            ros::VoidPtr(),
	            &this->rosQueue
		    );  
	      this->rosSub = this->rosNode->subscribe(so);

	      // Spin up the queue helper thread.	      
	      this->rosQueueThread =
	        std::thread(std::bind(&PedsimModelPlugin::QueueThread, this));

	      ROS_INFO_STREAM("Pedsim plugin attached to [" + this->model->GetName() + "]");
		
	    } // end Load()

		/// \brief callback function
		/// Convert from geometry_msgs/Pose to ignition/math/Pose3d for SetWorldPose
		private: void OnRosMsg(const pedsim_msgs::TrackedPersonsConstPtr &msg)
		{
			geometry_msgs::Pose gm_pose = msg->tracks[stoi(this->model->GetName())-1].pose.pose;
			// names start at 1, tracks[] starts at 0
		   	
	    	tf::Quaternion q(
	    		gm_pose.orientation.x,
	    		gm_pose.orientation.y,
	    		gm_pose.orientation.z,
	    		gm_pose.orientation.w);
	    	tf::Matrix3x3 m(q);
			double roll, pitch, yaw;
			m.getRPY(roll, pitch, yaw);
			yaw += M_PI/2; // adjust yaw so gazebo model matches msg TODO - why is this necessary ?
			ignition::math::Pose3d gz_pose(
		   		gm_pose.position.x, gm_pose.position.y, gm_pose.position.z,
		   		roll, pitch, yaw); 
			// set model pose
		   	this->model->SetWorldPose(gz_pose);
		} // end OnRosMsg()

	    /// \brief ROS helper function that processes messages
	    private: void QueueThread()
	    {
	      static const double timeout = 0.01;
	      while (this->rosNode->ok())
	      {
	        this->rosQueue.callAvailable(ros::WallDuration(timeout));
	      }
	    } // end QueueThread()

	    private:
	    		/// \brief Pointer to the model.
	    		physics::ModelPtr model;
				/// \brief A node use for ROS transport
	    		std::unique_ptr<ros::NodeHandle> rosNode;
			    /// \brief A ROS subscriber
				ros::Subscriber rosSub;
			    /// \brief A ROS callbackqueue that helps process messages
				ros::CallbackQueue rosQueue;
			    /// \brief A thread the keeps running the rosQueue
			    std::thread rosQueueThread;
	}; // end PedsimModelPlugin class

	// register plugin w/ Gazebo
	GZ_REGISTER_MODEL_PLUGIN(PedsimModelPlugin)

} // end gazebo namespace

#endif
