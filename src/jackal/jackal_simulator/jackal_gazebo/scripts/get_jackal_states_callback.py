#! /usr/bin/env python

import rospy

def get_model_states():
    model_coordinates = rospy.ServiceProxy( '/gazebo/get_model_state', GetModelState)
    object_coordinates = model_coordinates("jackal", "")
    z_position = object_coordinates.pose.position.z
    y_position = object_coordinates.pose.position.y
    x_position = object_coordinates.pose.position.x
    print("X: ",x_position)
    print("Y: ",y_position)
    print("Z: ",z_position)


if __name__ == '__main__':
    get_model_states()