#!/usr/bin/env python

import gym
import numpy
import time
from gym import wrappers

# Utilities
import os
import json
import liveplot
import deepq
import time
import csv
from distutils.dir_util import copy_tree
from datetime import datetime



# ROS packages required
import rospy
import rospkg
from openai_ros.openai_ros_common import StartOpenAI_ROS_Environment


def detect_monitor_files(training_dir):
    return [os.path.join(training_dir, f) for f in os.listdir(training_dir) if f.startswith('openaigym')]

def clear_monitor_files(training_dir):
    files = detect_monitor_files(training_dir)
    if len(files) == 0:
        return
    for file in files:
        print(file)
        os.unlink(file)

def create_csv(directory):
    with open(directory,'w') as file:
        writer = csv.DictWriter(file, fieldnames = ['epoch','reward'])
        writer.writeheader() 

def save_rewards(directory,epoch,reward):
    with open(directory,'a') as file:
        writer = csv.DictWriter(file, fieldnames = ['epoch','reward'])
        writer.writerow({'epoch':epoch,'reward':reward})

def read_waypoints(directory):
    with open(directory,'r') as file:
        reader = csv.DictReader(file)
        for row in reader:
            print(dict(row))

if __name__ == '__main__':

    rospy.init_node('Jackal_DQN',
                    anonymous=True, log_level=rospy.WARN)

    # Init OpenAI_ROS ENV
    task_and_robot_environment_name = rospy.get_param(
        '/jackal/task_and_robot_environment_name')
    env = StartOpenAI_ROS_Environment(
        task_and_robot_environment_name)
    # Create the Gym environment
    rospy.loginfo("Gym environment done")
    rospy.loginfo("Starting Learning")

    now = datetime.now()
    # Set the logging system
    rospack = rospkg.RosPack()
    pkg_path = rospack.get_path('my_jackal_dqn')
    outdir = pkg_path + '/training_results'
   # reward_file = pkg_path + '/training_results/'+str(now.hour)+'_'+str(now.minute)+'_reward.csv'
    reward_file = pkg_path + '/training_results/4_8_reward.csv'

    path = pkg_path + '/training_results/jackal_dqn_ep'
    plotter = liveplot.LivePlot(outdir)
    
    env = wrappers.Monitor(env, outdir, force=True)
    rospy.loginfo("Monitor Wrapper started")

    continue_execution = True #Change to false if you dont want to resume from last training episode and weights
    #fill this if continue_execution=True
    resume_epoch = '750' # change to epoch to continue from
    resume_path = path + resume_epoch
    weights_path = resume_path + '.h5'
    monitor_path = resume_path
    params_json  = resume_path + '.json'

    if not continue_execution:
        #Each time we take a sample and update our weights it is called a mini-batch.
        #Each time we run through the entire dataset, it's called an epoch.
        #PARAMETER LIST
        epochs = rospy.get_param("/jackal/epochs")
        steps = rospy.get_param("/jackal/steps")
        updateTargetNetwork = rospy.get_param("/jackal/updateTargetNetwork")
        explorationRate = rospy.get_param("/jackal/explorationRate")
        epsilon_decay = rospy.get_param("/jackal/epsilon_decay")
        minibatch_size = rospy.get_param("/jackal/minibatch_size")
        learnStart = rospy.get_param("/jackal/learnStart")
        learningRate = rospy.get_param("/jackal/learningRate")
        discountFactor = rospy.get_param("/jackal/discountFactor")
        memorySize = rospy.get_param("/jackal/memorySize")
        network_inputs = rospy.get_param("/jackal/network_inputs")
        network_outputs = rospy.get_param("/jackal/network_outputs")
        network_structure = rospy.get_param("/jackal/network_structure")
        current_epoch = rospy.get_param("/jackal/current_epoch")

        deepQ = deepq.DeepQ(network_inputs, network_outputs, memorySize, discountFactor, learningRate, learnStart)
        deepQ.initNetworks(network_structure)

        create_csv(reward_file)


    else: # Resume from last time we left off

        #Load weights, monitor info and parameter info.
        #ADD TRY CATCH fro this else
        with open(params_json) as outfile:
            d = json.load(outfile)
            epochs = d.get('epochs')
            steps = d.get('steps')
            updateTargetNetwork = d.get('updateTargetNetwork')
            explorationRate = d.get('explorationRate')
            epsilon_decay = d.get('epsilon_decay')
            minibatch_size = d.get('minibatch_size')
            learnStart = d.get('learnStart')
            learningRate = d.get('learningRate')
            discountFactor = d.get('discountFactor')
            memorySize = d.get('memorySize')
            network_inputs = d.get('network_inputs')
            network_outputs = d.get('network_outputs')
            network_structure = d.get('network_structure')
            current_epoch = d.get('current_epoch')

        deepQ = deepq.DeepQ(network_inputs, network_outputs, memorySize, discountFactor, learningRate, learnStart)
        deepQ.initNetworks(network_structure)
        deepQ.loadWeights(weights_path)

        clear_monitor_files(outdir)
        #copy_tree(monitor_path,outdir)

    env._max_episode_steps = steps # env returns done after _max_episode_steps
    env = gym.wrappers.Monitor(env, outdir,force=not continue_execution, resume=continue_execution)    

    last100Scores = [0] * 100
    last100ScoresIndex = 0
    last100Filled = False
    stepCounter = 0
    highest_reward = 0

    start_time = time.time()


    #start iterating from 'current epoch'.
    for epoch in xrange(current_epoch+1, epochs+1, 1):
        done = False
        observation = env.reset()
        print("Env has been reseted")
        cumulated_reward = 0
        print("Done is ", done)
        episode_step = 0
        # run until env returns done
        for i in range(steps):
            # env.render()
            qValues = deepQ.getQValues(observation)
            action = deepQ.selectAction(qValues, explorationRate)
            newObservation, reward, done, info = env.step(action)
    
            cumulated_reward += reward
            if highest_reward < cumulated_reward:
                highest_reward = cumulated_reward

            deepQ.addMemory(observation, action, reward, newObservation, done)

            if stepCounter >= learnStart:
                if stepCounter <= updateTargetNetwork:
                    deepQ.learnOnMiniBatch(minibatch_size, False)
                else :
                    deepQ.learnOnMiniBatch(minibatch_size, True)

            

            rospy.logwarn("# state we were=> " + str(observation))
            rospy.logwarn("# action that we took=> " + str(action))
            rospy.logwarn("# reward that action gave=> " + str(reward))
            rospy.logwarn("# episode cumulated_reward=> " + str(cumulated_reward))
            rospy.logwarn("# episode done=> " + str(done))
            rospy.logwarn("# episode=> " + str(epoch))
            rospy.logwarn("# steps=> " + str(i))
            
            observation = newObservation #Next State

            # if(episode_step == steps):
            #     env._is_done(True)
            #     done = True
                
            if done:
                rospy.logwarn("############### END OF EPISODE=>" + str(epoch))
                save_rewards(reward_file,epoch,cumulated_reward)

                last100Scores[last100ScoresIndex] = episode_step
                last100ScoresIndex += 1
                if last100ScoresIndex >= 100:
                    last100Filled = True
                    last100ScoresIndex = 0
                if not last100Filled:
                    print ("EP " + str(epoch) + " - " + format(episode_step + 1) + "/" + str(steps) + " Episode steps   Exploration=" + str(round(explorationRate, 2)))
                else :
                    m, s = divmod(int(time.time() - start_time), 60)
                    h, m = divmod(m, 60)
                    print ("EP " + str(epoch) + " - " + format(episode_step + 1) + "/" + str(steps) + " Episode steps - last100 Steps : " + str((sum(last100Scores) / len(last100Scores))) + " - Cumulated R: " + str(cumulated_reward) + "   Eps=" + str(round(explorationRate, 2)) + "     Time: %d:%02d:%02d" % (h, m, s))
                    if (epoch)%50==0:
                        #save model weights and monitoring data every 100 epochs.
                        rospy.logwarn("SAVING MODEL EPOCH: " + str(epoch))
                        deepQ.saveModel(path+str(epoch)+'.h5')
                        env._flush()
                        #copy_tree(outdir,path+str(epoch))
                        #save simulation parameters.
                        parameter_keys = ['epochs','steps','updateTargetNetwork','explorationRate','epsilon_decay','minibatch_size','learnStart','learningRate','discountFactor','memorySize','network_inputs','network_outputs','network_structure','current_epoch']
                        parameter_values = [epochs, steps, updateTargetNetwork, explorationRate,epsilon_decay, minibatch_size, learnStart, learningRate, discountFactor, memorySize, network_inputs, network_outputs, network_structure, epoch]
                        parameter_dictionary = dict(zip(parameter_keys, parameter_values))
                        with open(path+str(epoch)+'.json', 'w') as outfile:
                            json.dump(parameter_dictionary, outfile)



                       
            stepCounter += 1

            if stepCounter % updateTargetNetwork == 0:
                deepQ.updateTargetNetwork()
                print ("updating target network")

            if(done):
                break

            

            episode_step += 1


        explorationRate *= epsilon_decay #epsilon decay
        # explorationRate -= (2.0/epochs)
        explorationRate = max(0.05, explorationRate) #minimum exploration rate is 0.05

        if epoch % 100 == 0:
            plotter.plot(env)

    env.close()
