#! /usr/bin/env python
import rospy
from gazebo_msgs.msg import ModelStates

def _gazebo_model_states_callback(gazebo_state):
        jackal_state =  gazebo_state.pose[-1]
        print(str(jackal_state.position))

if __name__ == '__main__':
    rospy.init_node('listener', anonymous=True)
    rospy.Subscriber("/gazebo/model_states", ModelStates,_gazebo_model_states_callback)
    rospy.spin()
