import rospy
import numpy
import time
import math
import random
from gym import spaces
from openai_ros.robot_envs import jackal_dqn_env
from gym.envs.registration import register
from sensor_msgs.msg import LaserScan
from std_msgs.msg import Header

from geometry_msgs.msg import Twist, Point, Pose
from gazebo_msgs.srv import SpawnModel, DeleteModel


from openai_ros.task_envs.task_commons import LoadYamlFileParamsTest
from openai_ros.openai_ros_common import ROSLauncher
import os



class JackalDQNWorldEnv(jackal_dqn_env.JackalDQNEnv):
    def __init__(self):
 
        ros_ws_abspath = rospy.get_param("/jackal/ros_ws_abspath", None)
        assert ros_ws_abspath is not None, "You forgot to set ros_ws_abspath in your yaml file of your main RL script. Set ros_ws_abspath: \'YOUR/SIM_WS/PATH\'"
        assert os.path.exists(ros_ws_abspath), "The Simulation ROS Workspace path "+ros_ws_abspath + " DOESNT exist, execute: mkdir -p "+ros_ws_abspath + "/src;cd "+ros_ws_abspath+";catkin_make"
        ROSLauncher(rospackage_name="pedsim_gazebo_plugin",
                    launch_file_name="circuit_world.launch",
                    ros_ws_abspath=ros_ws_abspath)

        # Load Params from the desired Yaml file
        LoadYamlFileParamsTest(rospackage_name="openai_ros",
                                    rel_path_from_package_to_file="src/openai_ros/task_envs/jackal_dqn/config",
                                    yaml_file_name="jackal_navigation.yaml")

        # Here we will add any init functions prior to starting the MyRobotEnv
        super(JackalDQNWorldEnv, self).__init__(ros_ws_abspath)

         # Only variable needed to be set here
        number_actions = rospy.get_param('/jackal/n_actions')
        self.action_space = spaces.Discrete(number_actions)

        # We set  range, which is not compulsory but here we do it.
        self.reward_range = (-numpy.inf, numpy.inf)
        self.crashed = False

        # Actions and Observations
        self.dec_obs = rospy.get_param("/jackal/number_decimals_precision_obs", 1)
        self.linear_forward_speed = rospy.get_param('/jackal/linear_forward_speed')
        self.linear_turn_speed = rospy.get_param('/jackal/linear_turn_speed')
        self.angular_speed = rospy.get_param('/jackal/angular_speed')
        self.init_linear_forward_speed = rospy.get_param('/jackal/init_linear_forward_speed')
        self.init_linear_turn_speed = rospy.get_param('/jackal/init_linear_turn_speed')
        self.goal_x = rospy.get_param('/jackal/goal_x')
        self.goal_y = rospy.get_param('/jackal/goal_y')
        self.goal_epsilon = rospy.get_param('jackal/goal_epsilon')
        self.goal_reward = rospy.get_param('jackal/goal_reward')
        self.goal_scaling_reward = rospy.get_param('jackal/goal_scaling_reward')
        self.n_observations = rospy.get_param('/jackal/n_observations')  # We have 6 different observations #input layer size
        self.min_range = rospy.get_param('/jackal/min_range') # Minimum meters below wich we consider we have crashed
        self.max_laser_value = rospy.get_param('/jackal/max_laser_value') # Value considered Ok, no wall #using
        self.min_laser_value = rospy.get_param('/jackal/min_laser_value')  # Value considered there is an obstacle or crashed #using

        #Laserscan
        laser_scan = self.get_laser_scan()
        self.laser_scan_frame = laser_scan.header.frame_id


        #Goal
        self.goal_position = Pose()
        self.goal_position.position.x = self.goal_x
        self.goal_position.position.y = self.goal_y

        self.goal = rospy.ServiceProxy('/gazebo/spawn_sdf_model', SpawnModel)
        self.del_model = rospy.ServiceProxy('/gazebo/delete_model', DeleteModel)
        self.goal_model_dir = "/home/dngu0016/fyp_ws/src/turtlebot3/turtlebot3_simulations/turtlebot3_gazebo/models/Target/model.sdf"

    
        
        # Number of laser reading jumped
        self.new_ranges = int(
            math.ceil(float(len(laser_scan.ranges)) / float(self.n_observations))) #I: recevied the number of ranges per observation
        #if there are 360 ranges and 6 observation, it would be 60 per observation. so new_ranges = 60
        # rospy.logdebug("n_observations===>"+str(self.n_observations))
        # rospy.logdebug(
        #     "new_ranges, jumping laser readings===>"+str(self.new_ranges))
        

        # Rewards
        self.forwards_reward = rospy.get_param("/jackal/forwards_reward")
        self.turn_reward = rospy.get_param("/jackal/turn_reward")
        self.crash_reward = rospy.get_param("/jackal/crash_reward")
        self.end_episode_points = rospy.get_param("/jackal/end_episode_points")
        self.laser_filtered_pub = rospy.Publisher('/jackal/laser/scan_filtered', LaserScan, queue_size=1)
        self.seed()

        self.cumulated_steps = 0.0
        self.prev_distance = 0.0
        self.curr_distance = 0.0


    def _set_init_pose(self):
        """Sets the Robot in its init pose
        """
        self.move_base(self.init_linear_forward_speed,
                       self.init_linear_turn_speed,
                       epsilon=0.05,
                       update_rate=10,
                       min_laser_distance=1.1)

        return True

    def _init_env_variables(self):
        """
        Inits variables needed to be initialised each time we reset at the start
        of an episode.
        :return:
        """
        self.cumulated_reward = 0.0
        self.prev_distance = 0.0
        self._episode_done = False

        rospy.wait_for_service('/gazebo/spawn_sdf_model')
        try:
            goal_urdf = open(self.goal_model_dir, "r").read()
            target = SpawnModel
            target.model_name = 'target'  # the same with sdf name
            target.model_xml = goal_urdf
            self.goal_position.position.x = random.uniform(-3.6, 3.6)
            self.goal_position.position.y = random.uniform(-3.6, 3.6)
            self.goal(target.model_name, target.model_xml, 'namespace', self.goal_position, 'world')
        except (rospy.ServiceException) as e:
            print("/gazebo/failed to build the target")

        # rospy.wait_for_service('/gazebo/unpause_physics')
        self.goal_distance = self.getGoalDistace()


        # We wait a small ammount of time to start everything because in very fast resets, laser scan values are sluggish
        # and sometimes still have values from the prior position that triguered the done.
        time.sleep(1.0)

        # TODO: Add reset of published filtered laser readings
        # laser_scan = self.get_laser_scan()
        # discretized_ranges = laser_scan.ranges
    
        # self.publish_filtered_laser_scan(laser_original_data=laser_scan,
        #                                  new_filtered_laser_range=discretized_ranges)


    def _set_action(self, action):
        """
        This set action will Set the linear and angular speed of the turtlebot2
        based on the action number given.
        :param action: The action integer that set s what movement to do next.
        """
        rospy.logdebug("Start Set Action ==>"+str(action))
        # We convert the actions to speed movements to send to the parent class CubeSingleDiskEnv
        if action == 0:  # FORWARD
            linear_speed = self.linear_forward_speed
            angular_speed = 0.0
            self.last_action = "FORWARDS"
        elif action == 1:  # LEFT
            linear_speed = self.linear_turn_speed
            angular_speed = self.angular_speed
            self.last_action = "TURN_LEFT"
        elif action == 2:  # RIGHT
            linear_speed = self.linear_turn_speed
            angular_speed = -1*self.angular_speed
            self.last_action = "TURN_RIGHT"
        # elif action == 3:  # BACK
        #     linear_speed = -1*self.linear_forward_speed
        #     angular_speed = 0
        #     self.last_action = "BACKWARDS"

        

        # We tell TurtleBot2 the linear and angular speed to set to execute
        self.move_base(linear_speed,
                       angular_speed,
                       epsilon=0.05,
                       update_rate=10,
                       min_laser_distance=self.min_range)

        rospy.logdebug("END Set Action ==>"+str(action) +
                       ", NAME="+str(self.last_action))

    def _get_obs(self):
        """
        Here we define what sensor data defines our robots observations
        To know which Variables we have acces to, we need to read the
        TurtleBot2Env API DOCS
        :return:

        16 Dimensions: 10 Laser State, Past Action, Target relative distance, Target relative angle, robot yaw,
        """

        scan_range = []
        yaw = round(self.yaw,self.dec_obs)
        rel_theta = round(self.rel_theta,self.dec_obs)
        diff_angle = round(self.diff_angle,self.dec_obs)
        unfilt_laser_scan = self.get_laser_scan()

        for i in range(len(unfilt_laser_scan)):
            if unfilt_laser_scan[i] == float('Inf'):
                scan_range.append(self.max_laser_value)
            elif numpy.isnan(unfilt_laser_scan[i]):
                scan_range.append(0)
            else:
                scan_range.append(round(unfilt_laser_scan[i],self.dec_obs))

        rospy.logdebug("Start Get Observation ==>")
        # We get the laser scan data
        current_distance = math.hypot(self.goal_position.position.x - self.jackal_x, self.goal_position.position.y - self.jackal_y)
        current_distance = round(current_distance,self.dec_obs)
        state = [current_distance, yaw, rel_theta, diff_angle]

        scan_range.extend(state)
        numpy_arr = numpy.array(scan_range)

        rospy.logdebug("Observations==>"+str(numpy_arr))
        rospy.logdebug("AFTER DISCRET_episode_done==>"+str(self._episode_done))
        rospy.logdebug("END Get Observation ==>")
        return numpy_arr


    def _is_done(self, eps_timeout = False):

        if(eps_timeout == True):
            self._episode_done = True

        if self._episode_done:
            rospy.logdebug("Jackal is Too Close to wall==>" +
                           str(self._episode_done))
                           
        else:
            rospy.logerr("Jackal is Ok ==>")

        rospy.wait_for_service('/gazebo/delete_model')
        self.del_model('target')

        return self._episode_done

    def _compute_reward(self, observations, done):

        reward = 0

        if not done:
            self.curr_distance = math.sqrt((self.goal_x-self.jackal_x)**2 + (self.goal_y-self.jackal_y)**2)

            if((abs(self.jackal_x - self.goal_x) < self.goal_epsilon) and (abs(self.jackal_y - self.goal_y) < self.goal_epsilon)):
                reward += 1500
            else:
                if((self.curr_distance + 0.01) < self.prev_distance):
                    reward += 5
                else:
                    reward += -5

        else:
            reward = -1*self.end_episode_points
            
        if self.has_crashed(min_laser_distance=self.min_range):
            reward = -1000

        rospy.logdebug("reward=" + str(reward))
        self.cumulated_reward += reward
        rospy.logdebug("Cumulated_reward=" + str(self.cumulated_reward))
        self.cumulated_steps += 1
        rospy.logdebug("Cumulated_steps=" + str(self.cumulated_steps))

        self.prev_distance = self.curr_distance

        return reward


    # Internal TaskEnv Methods

    def getGoalDistace(self):
        goal_distance = math.hypot(self.goal_position.position.x - self.position.x, self.goal_position.position.y - self.position.y)
        self.past_distance = goal_distance

        return goal_distance

    def getOdometry(self):
        self.position = self.odom.pose.pose.position
        orientation = self.odom.pose.pose.orientation
        q_x, q_y, q_z, q_w = orientation.x, orientation.y, orientation.z, orientation.w
        yaw = round(math.degrees(math.atan2(2 * (q_x * q_y + q_w * q_z), 1 - 2 * (q_y * q_y + q_z * q_z))))

        if yaw >= 0:
             yaw = yaw
        else:
             yaw = yaw + 360

        rel_dis_x = round(self.goal_position.position.x - self.position.x, 1)
        rel_dis_y = round(self.goal_position.position.y - self.position.y, 1)

        # Calculate the angle between robot and target
        if rel_dis_x > 0 and rel_dis_y > 0:
            theta = math.atan(rel_dis_y / rel_dis_x)
        elif rel_dis_x > 0 and rel_dis_y < 0:
            theta = 2 * math.pi + math.atan(rel_dis_y / rel_dis_x)
        elif rel_dis_x < 0 and rel_dis_y < 0:
            theta = math.pi + math.atan(rel_dis_y / rel_dis_x)
        elif rel_dis_x < 0 and rel_dis_y > 0:
            theta = math.pi + math.atan(rel_dis_y / rel_dis_x)
        elif rel_dis_x == 0 and rel_dis_y > 0:
            theta = 1 / 2 * math.pi
        elif rel_dis_x == 0 and rel_dis_y < 0:
            theta = 3 / 2 * math.pi
        elif rel_dis_y == 0 and rel_dis_x > 0:
            theta = 0
        else:
            theta = math.pi
        rel_theta = round(math.degrees(theta), 2)

        diff_angle = abs(rel_theta - yaw)

        if diff_angle <= 180:
            diff_angle = round(diff_angle, 2)
        else:
            diff_angle = round(360 - diff_angle, 2)

        self.rel_theta = rel_theta
        self.yaw = yaw
        self.diff_angle = diff_angle

    # def discretize_observation(self, data, new_ranges):
    #     """
    #     Discards all the laser readings that are not multiple in index of new_ranges
    #     value. new_ranges = 121.
    #     """
    #     #print(new_ranges)
    #     input_layer = 86
    #     observation_index = range(0,len(data.ranges),int(math.ceil((len(data.ranges) - 0)/input_layer)))
    #     discretized_ranges = []
    #     filtered_range = []
    #     #mod = len(data.ranges)/new_ranges
    #     mod = new_ranges

    #     max_laser_value = data.range_max
    #     min_laser_value = data.range_min

    #     rospy.logdebug("data=" + str(data))
    #     # rospy.logwarn("mod=" + str(mod))
   

    #     for i, item in enumerate(data.ranges):
    #         if (i % mod == 0):
    #             if  item == float('Inf')  or numpy.isinf(item):
    #                 # discretized_ranges.append(self.max_laser_value)
    #                 discretized_ranges.append(
    #                     round(max_laser_value, self.dec_obs))
    #             elif numpy.isnan(item):
    #                 # discretized_ranges.append(self.min_laser_value)
    #                 discretized_ranges.append(
    #                     round(min_laser_value, self.dec_obs))
    #             else:
    #                 # discretized_ranges.append(int(item))
    #                 discretized_ranges.append(round(item, self.dec_obs))

    #             if (self.min_range > item > 0):
    #                # rospy.logerr("done Validation >>> item=" +
    #                                 #str(item)+"< "+str(self.min_range))
    #                 self._episode_done = True
    #             #else:
    #                 #rospy.logwarn("NOT done Validation >>> item=" +
    #                                 #str(item)+"< "+str(self.min_range))
    #             # We add last value appended
    #             filtered_range.append(discretized_ranges[-1])
    #         else:
    #             # We add value zero
    #             filtered_range.append(0)
            

    #     rospy.logdebug(
    #         "Size of observations, discretized_ranges==>"+str(len(discretized_ranges)))

    #     self.publish_filtered_laser_scan(laser_original_data=data,
    #                                      new_filtered_laser_range=discretized_ranges)

    #     return discretized_ranges



    # def publish_filtered_laser_scan(self, laser_original_data, new_filtered_laser_range):

    #     rospy.logdebug("new_filtered_laser_range==>" +
    #                    str(new_filtered_laser_range))

    #     laser_filtered_object = LaserScan()

    #     h = Header()
    #     # Note you need to call rospy.init_node() before this will work
    #     h.stamp = rospy.Time.now()
    #     h.frame_id = laser_original_data.header.frame_id

    #     laser_filtered_object.header = h
    #     laser_filtered_object.angle_min = laser_original_data.angle_min
    #     laser_filtered_object.angle_max = laser_original_data.angle_max

    #     new_angle_incr = abs(laser_original_data.angle_max -
    #                          laser_original_data.angle_min) / len(new_filtered_laser_range)

    #     #laser_filtered_object.angle_increment = laser_original_data.angle_increment
    #     laser_filtered_object.angle_increment = new_angle_incr
    #     laser_filtered_object.time_increment = laser_original_data.time_increment
    #     laser_filtered_object.scan_time = laser_original_data.scan_time
    #     laser_filtered_object.range_min = laser_original_data.range_min
    #     laser_filtered_object.range_max = laser_original_data.range_max

    #     laser_filtered_object.ranges = []
    #     laser_filtered_object.intensities = []
    #     for item in new_filtered_laser_range:
    #         if item == 0.0:
    #             laser_distance = 0.1
    #         else:
    #             laser_distance = item
    #         laser_filtered_object.ranges.append(laser_distance)
    #         laser_filtered_object.intensities.append(item)

    #     self.laser_filtered_pub.publish(laser_filtered_object)