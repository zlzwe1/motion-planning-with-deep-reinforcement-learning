import tensorflow as tf
import keras
from tensorflow.python.client import device_lib
from keras import backend as K
print(device_lib.list_local_devices())
K.tensorflow_backend._get_available_gpus()

config = tf.ConfigProto( device_count = {'GPU':1})
sess = tf.Session(config=config)
keras.backend.set_session(sess)

print("Num GPUs Available", len(tf.config.experimental.list_physical_devices('GPU')))

tf.debugging.set_log_device_placement(True)

# Create some tensors
a = tf.constant([[1.0, 2.0, 3.0], [4.0, 5.0, 6.0]])
b = tf.constant([[1.0, 2.0], [3.0, 4.0], [5.0, 6.0]])
c = tf.matmul(a, b)

print(c)