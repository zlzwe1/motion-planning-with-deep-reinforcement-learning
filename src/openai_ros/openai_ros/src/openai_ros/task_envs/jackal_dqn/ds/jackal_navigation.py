import rospy
import numpy
import time
import math
from gym import spaces
from openai_ros.robot_envs import jackal_dqn_env
from gym.envs.registration import register
from sensor_msgs.msg import LaserScan
from std_msgs.msg import Header

from openai_ros.task_envs.task_commons import LoadYamlFileParamsTest
from openai_ros.openai_ros_common import ROSLauncher
import os



class JackalDQNWorldEnv(jackal_dqn_env.JackalDQNEnv):
    def __init__(self):
 
        ros_ws_abspath = rospy.get_param("/jackal/ros_ws_abspath", None)
        assert ros_ws_abspath is not None, "You forgot to set ros_ws_abspath in your yaml file of your main RL script. Set ros_ws_abspath: \'YOUR/SIM_WS/PATH\'"
        assert os.path.exists(ros_ws_abspath), "The Simulation ROS Workspace path "+ros_ws_abspath + " DOESNT exist, execute: mkdir -p "+ros_ws_abspath + "/src;cd "+ros_ws_abspath+";catkin_make"
        ROSLauncher(rospackage_name="pedsim_gazebo_plugin",
                    launch_file_name="circuit_world.launch",
                    ros_ws_abspath=ros_ws_abspath)

        # Load Params from the desired Yaml file
        LoadYamlFileParamsTest(rospackage_name="openai_ros",
                                    rel_path_from_package_to_file="src/openai_ros/task_envs/jackal_dqn/config",
                                    yaml_file_name="jackal_navigation.yaml")

        # Here we will add any init functions prior to starting the MyRobotEnv
        super(JackalDQNWorldEnv, self).__init__(ros_ws_abspath)

         # Only variable needed to be set here
        number_actions = rospy.get_param('/jackal/n_actions')
        self.action_space = spaces.Discrete(number_actions)

        # We set  range, which is not compulsory but here we do it.
        self.reward_range = (-numpy.inf, numpy.inf)
        self.crashed = False

        # Actions and Observations
        self.dec_obs = rospy.get_param(
            "/jackal/number_decimals_precision_obs", 1)
        self.linear_forward_speed = rospy.get_param(
            '/jackal/linear_forward_speed')
        self.linear_turn_speed = rospy.get_param(
            '/jackal/linear_turn_speed')
        self.angular_speed = rospy.get_param('/jackal/angular_speed')
        self.init_linear_forward_speed = rospy.get_param(
            '/jackal/init_linear_forward_speed')
        self.init_linear_turn_speed = rospy.get_param(
            '/jackal/init_linear_turn_speed')
        self.goal_x = rospy.get_param('/jackal/goal_x')
        self.goal_y = rospy.get_param('/jackal/goal_y')
        self.goal_epsilon = rospy.get_param('jackal/goal_epsilon')
        self.goal_reward = rospy.get_param('jackal/goal_reward')
        self.goal_scaling_reward = rospy.get_param('jackal/goal_scaling_reward')

        self.n_observations = rospy.get_param('/jackal/n_observations')  # We have 6 different observations #input layer size
        self.min_range = rospy.get_param('/jackal/min_range') # Minimum meters below wich we consider we have crashed
        self.max_laser_value = rospy.get_param('/jackal/max_laser_value') # Value considered Ok, no wall #using
        self.min_laser_value = rospy.get_param('/jackal/min_laser_value')  # Value considered there is an obstacle or crashed #using

        # We create two arrays based on the binary values that will be assigned
        # In the discretization method.
        #laser_scan = self._check_laser_scan_ready()
        laser_scan = self.get_laser_scan()
        rospy.logdebug("laser_scan len===>"+str(len(laser_scan.ranges)))

        # Laser data
        self.laser_scan_frame = laser_scan.header.frame_id

        self.prev_distance = 0.0
        self.curr_distance = 0.0

        
        # Number of laser reading jumped
        self.new_ranges = int(
            math.ceil(float(len(laser_scan.ranges)) / float(self.n_observations))) #I: recevied the number of ranges per observation
        #if there are 360 ranges and 6 observation, it would be 60 per observation. so new_ranges = 60
        rospy.logdebug("n_observations===>"+str(self.n_observations))
        rospy.logdebug(
            "new_ranges, jumping laser readings===>"+str(self.new_ranges))

        # high = numpy.full((self.n_observations), self.max_laser_value)
        # low = numpy.full((self.n_observations), self.min_laser_value)

        # # We only use two integers
        # self.observation_space = spaces.Box(low, high)

        

        # Rewards
        self.forwards_reward = rospy.get_param("/jackal/forwards_reward")
        self.turn_reward = rospy.get_param("/jackal/turn_reward")
        self.crash_reward = rospy.get_param("/jackal/crash_reward")
        self.end_episode_points = rospy.get_param(
            "/jackal/end_episode_points")

        self.cumulated_steps = 0.0

        self.laser_filtered_pub = rospy.Publisher(
            '/jackal/laser/scan_filtered', LaserScan, queue_size=1)

        self.seed()


    def _set_init_pose(self):
        """Sets the Robot in its init pose
        """
        self.move_base(self.init_linear_forward_speed,
                       self.init_linear_turn_speed,
                       epsilon=0.05,
                       update_rate=10,
                       min_laser_distance=1.1)

        return True

    def _init_env_variables(self):
        """
        Inits variables needed to be initialised each time we reset at the start
        of an episode.
        :return:
        """
        # For Info Purposes
        self.cumulated_reward = 0.0
        # Set to false Done, because its calculated asyncronously
        self._episode_done = False
        self.prev_distance = 0.0

        # We wait a small ammount of time to start everything because in very fast resets, laser scan values are sluggish
        # and sometimes still have values from the prior position that triguered the done.
        time.sleep(1.0)

        # TODO: Add reset of published filtered laser readings
        laser_scan = self.get_laser_scan()
        discretized_ranges = laser_scan.ranges
    
        self.publish_filtered_laser_scan(laser_original_data=laser_scan,
                                         new_filtered_laser_range=discretized_ranges)


    def _set_action(self, action):
        """
        This set action will Set the linear and angular speed of the turtlebot2
        based on the action number given.
        :param action: The action integer that set s what movement to do next.
        """
        rospy.logdebug("Start Set Action ==>"+str(action))
        # We convert the actions to speed movements to send to the parent class CubeSingleDiskEnv
        if action == 0:  # FORWARD
            linear_speed = self.linear_forward_speed
            angular_speed = 0.0
            self.last_action = "FORWARDS"
        elif action == 1:  # LEFT
            linear_speed = self.linear_turn_speed
            angular_speed = self.angular_speed
            self.last_action = "TURN_LEFT"
        elif action == 2:  # RIGHT
            linear_speed = self.linear_turn_speed
            angular_speed = -1*self.angular_speed
            self.last_action = "TURN_RIGHT"
        elif action == 3:  # BACK
            linear_speed = -1*self.linear_forward_speed
            angular_speed = 0
            self.last_action = "BACKWARDS"

        

        # We tell TurtleBot2 the linear and angular speed to set to execute
        self.move_base(linear_speed,
                       angular_speed,
                       epsilon=0.05,
                       update_rate=10,
                       min_laser_distance=self.min_range)

        rospy.logdebug("END Set Action ==>"+str(action) +
                       ", NAME="+str(self.last_action))

    def _get_obs(self):
        """
        Here we define what sensor data defines our robots observations
        To know which Variables we have acces to, we need to read the
        TurtleBot2Env API DOCS
        :return:
        """

        rospy.logdebug("Start Get Observation ==>")
        # We get the laser scan data
        laser_scan = self.get_laser_scan()

        rospy.logdebug("BEFORE DISCRET _episode_done==>" +
                       str(self._episode_done))

        discretized_observations = self.discretize_observation(laser_scan,
                                                               self.new_ranges
                                                               )

        rospy.logdebug("Observations==>"+str(discretized_observations))
        rospy.logdebug("AFTER DISCRET_episode_done==>"+str(self._episode_done))
        rospy.logdebug("END Get Observation ==>")

        position_state = [round(self.jackal_x, self.dec_obs),round(self.jackal_y, self.dec_obs),self.goal_x,self.goal_y]

        discretized_observations.extend(position_state)
        

        numpy_arr = numpy.array(discretized_observations)
        return numpy_arr


    def _compute_reward(self, observations, done):

        reward = 0

        if not done:
            self.curr_distance = math.sqrt((self.goal_x-self.jackal_x)**2 + (self.goal_y-self.jackal_y)**2)

            if((abs(self.jackal_x - self.goal_x) < self.goal_epsilon) and (abs(self.jackal_y - self.goal_y) < self.goal_epsilon)):
                reward += 1500
            else:
                if((self.curr_distance + 0.01) < self.prev_distance):
                    reward += 5
                else:
                    reward += -5


            if self.last_action == "FORWARDS":
                reward += 0.5
            elif self.last_action == "BACKWARDS":
                reward += -0.5
            else:
                reward += 0.25
            
        else:
            reward = -1*self.end_episode_points
            
        if self.has_crashed(min_laser_distance=self.min_range):
            reward = -1000

        rospy.logdebug("reward=" + str(reward))
        self.cumulated_reward += reward
        rospy.logdebug("Cumulated_reward=" + str(self.cumulated_reward))
        self.cumulated_steps += 1
        rospy.logdebug("Cumulated_steps=" + str(self.cumulated_steps))

        self.prev_distance = self.curr_distance

        return reward


    # Internal TaskEnv Methods

    def discretize_observation(self, data, new_ranges):
        """
        Discards all the laser readings that are not multiple in index of new_ranges
        value. new_ranges = 121.
        """
        #print(new_ranges)
        input_layer = 86
        observation_index = range(0,len(data.ranges),int(math.ceil((len(data.ranges) - 0)/input_layer)))
        discretized_ranges = []
        filtered_range = []
        #mod = len(data.ranges)/new_ranges
        mod = new_ranges

        max_laser_value = data.range_max
        min_laser_value = data.range_min

        rospy.logdebug("data=" + str(data))
        # rospy.logwarn("mod=" + str(mod))
   
        selected_ranges = numpy.array(data.ranges)[observation_index]
        

        for i, item in enumerate(data.ranges):
            if (i % mod == 0):
                if  item == float('Inf')  or numpy.isinf(item):
                    # discretized_ranges.append(self.max_laser_value)
                    discretized_ranges.append(
                        round(max_laser_value, self.dec_obs))
                elif numpy.isnan(item):
                    # discretized_ranges.append(self.min_laser_value)
                    discretized_ranges.append(
                        round(min_laser_value, self.dec_obs))
                else:
                    # discretized_ranges.append(int(item))
                    discretized_ranges.append(round(item, self.dec_obs))

                if (self.min_range > item > 0):
                   # rospy.logerr("done Validation >>> item=" +
                                    #str(item)+"< "+str(self.min_range))
                    self._episode_done = True
                #else:
                    #rospy.logwarn("NOT done Validation >>> item=" +
                                    #str(item)+"< "+str(self.min_range))
                # We add last value appended
                filtered_range.append(discretized_ranges[-1])
            else:
                # We add value zero
                filtered_range.append(0)
            

        rospy.logdebug(
            "Size of observations, discretized_ranges==>"+str(len(discretized_ranges)))

        self.publish_filtered_laser_scan(laser_original_data=data,
                                         new_filtered_laser_range=discretized_ranges)

        return discretized_ranges

    def publish_filtered_laser_scan(self, laser_original_data, new_filtered_laser_range):

        rospy.logdebug("new_filtered_laser_range==>" +
                       str(new_filtered_laser_range))

        laser_filtered_object = LaserScan()

        h = Header()
        # Note you need to call rospy.init_node() before this will work
        h.stamp = rospy.Time.now()
        h.frame_id = laser_original_data.header.frame_id

        laser_filtered_object.header = h
        laser_filtered_object.angle_min = laser_original_data.angle_min
        laser_filtered_object.angle_max = laser_original_data.angle_max

        new_angle_incr = abs(laser_original_data.angle_max -
                             laser_original_data.angle_min) / len(new_filtered_laser_range)

        #laser_filtered_object.angle_increment = laser_original_data.angle_increment
        laser_filtered_object.angle_increment = new_angle_incr
        laser_filtered_object.time_increment = laser_original_data.time_increment
        laser_filtered_object.scan_time = laser_original_data.scan_time
        laser_filtered_object.range_min = laser_original_data.range_min
        laser_filtered_object.range_max = laser_original_data.range_max

        laser_filtered_object.ranges = []
        laser_filtered_object.intensities = []
        for item in new_filtered_laser_range:
            if item == 0.0:
                laser_distance = 0.1
            else:
                laser_distance = item
            laser_filtered_object.ranges.append(laser_distance)
            laser_filtered_object.intensities.append(item)

        self.laser_filtered_pub.publish(laser_filtered_object)